<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://codex.management
 * @since      1.0.0
 *
 * @package    Codex_Connector
 * @subpackage Codex_Connector/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">
	<h2><?php echo esc_html(get_admin_page_title()); ?></h2>
	<p>
		<a href="https://codexapp.nl/handleiding/0revision1-15/" target="_blank">Lees hier alle instructies</a>
		om deze Wordpress Plug-in te gebruiken
	</p>
	<form action="options.php" method="post">
		<?php
		settings_fields($this->plugin_name);
		do_settings_sections($this->plugin_name);
		?>
		<?php
		submit_button();
		?>
	</form>

</div>
<div class="wrap">
	<h2>Verbinding status</h2>
	<?php if ($this->api_connection === false) {

	?>
		<p>Er kon geen verbinding gemaakt worden. Probeer het nog eens! Hieronder staan hints wat er mis is:</p>
		<p>
			<code><?php
					echo ($this->api_error);
					?></code>

		</p>
	<?php
	} else {

	?>
		<p>
			✓ Er is een verbinding.
		</p>
		<div>Huidige token value:

			<?php
			if (isset($_SESSION['codex_api_token'])) {
				echo "<code>" . $_SESSION['codex_api_token'] . "</code>";
			} else {
				echo "Geen token set in sessie";
			} ?>
		</div>
	<?php } ?>

</div>

<?php